This project revolves around the idea of building a database of wines and subjective opinions
about them as to increase my knowledge of wines.

Intended Features:
- data includes price, type/grape, brand/winery, barcode, online link(s), etc.
- final rating (this is subjectively based, however its value changes as more wines are added,
		meaning that wines you thought were good are now less so in comparison to
		newer, better wines)
- CLI using BASH (possibly a GUI later on)
- text based database, so that it can be shared easily using git, dropbox, etc.

if anyone has any suggestions, leave a issue :D
